from sys import argv

isingther = argv[0]
n = int(argv[1])
t = int(argv[2])
x = int(argv[3])
q = int(argv[4])

import numpy as np
import random
import matplotlib.pyplot as plt
import xlwt


def ising(n, t, x, q):
    """Produces a 2D ising model showing +/-1 values relative to the spin states

    Produces a temperature dependant 2D ising model following a metropolis algorithm with random number generation to...
    decide the progression of the system
    :param n: number of iterations
    :param t: End temperature of system
    :param x: no. of cols and rows in matrix
    :param q: no. of equilibrium steps
    :return: 2D array with values of +/-1
    :rtype : bytearray
    """
    wb = xlwt.Workbook()  # Setting up spreadsheet to write data too
    ws = wb.add_sheet("Ising", cell_overwrite_ok=True)
    ws.write(0, 0, "Temperature")
    ws.write(0, 1, "Total energy")
    ws.write(0, 2, "Cv")
    row = 2

    for temperature in np.arange(0.1, t, 0.1):  # Iterates over temperature increments
        spinarray = np.ones((x, x), dtype=np.int)  # Generates array of aligned spins of values +1
        Esa = 4*(x*x)  # Calculate total energy of system, /2 for double counting and x4 for energy of each site
        Esq = Esa*Esa # Calculate the energy squared for calculation of Cv
        Eavg = 0

        for itt in range(0, n):
            a = random.randint(0, (x-1))  # Generates random numbers for selection of random sites
            b = random.randint(0, (x-1))
            c = spinarray.item(a, b)  # Selects random spin site

            rnn = spinarray.item((a+1) % x, b)  # Imposing PBC
            lnn = spinarray.item((a-1) % x, b)
            dnn = spinarray.item(a, (b+1) % x)
            unn = spinarray.item(a, (b-1) % x)

            deltaE = -2.*c*(rnn + lnn + unn + dnn)  # Calculate energy change from nearest neighbour interactions

            if deltaE < 0.:
                spinarray[a, b] = c*(-1)  # Decrease in energy results in a spinflip
                newEsa = 2.*deltaE + Esa  # Updates lattice energy with the change
            else:
                d = random.random()  # Random number generated for comparison between 0 and 1
                p = np.exp(-deltaE/temperature)  # Probability of spin changing

                if d > p:  # Comparison of random number and probability
                    spinarray[a, b] = c  # State change rejected as probability too low
                    newEsa = Esa  # Counts the same lattice energy again
                else:
                    spinarray[a, b] = c*(-1)  # State change accepted as probability is high
                    newEsa = 2*deltaE + Esa  # Updates lattice energy with the change

            if itt > q:
                Esq += newEsa*newEsa*((x*x)**2)  # Adds the new energy squared for the squared average
                Eavg += newEsa/(x*x)  # Adds the new energy to the average

            Esa = newEsa  # Updates total lattice energy for next step

        E = (Eavg/(n-q))  # Average energy of the system per spin
        Cv = (Esq - (Eavg**2))/(n-q)*(temperature**2)  # Specific Heat of the system

        plt.scatter(temperature, Cv, 5)

        ws.write(row, 1, E)  # Records value for energy in Ising spreadsheet
        ws.write(row, 0, temperature)  # Records corresponding temperature value
        ws.write(row, 2, Cv)  # Records corresponding specific heat value
        row += 1

    plt.show()

    print spinarray

    wb.save('Ising.xls')

if __name__ == '__main__':
    ising(n, t, x, q)